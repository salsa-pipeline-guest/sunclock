Source: sunclock
Section: x11
Priority: optional
Maintainer: Roland Rosenfeld <roland@debian.org>
Standards-Version: 4.3.0
Build-Depends: debhelper (>= 12~),
               imagemagick,
               libjpeg-dev,
               libpng-dev,
               libx11-dev,
               libxpm-dev,
               xutils-dev
Homepage: https://github.com/mdoege/Sunclock
Vcs-Git: https://salsa.debian.org/debian/sunclock.git
Vcs-Browser: https://salsa.debian.org/debian/sunclock
Rules-Requires-Root: no

Package: sunclock
Architecture: any
Depends: sunclock-maps, ${misc:Depends}, ${shlibs:Depends}
Suggests: imagemagick
Description: fancy clock showing time and geographical data
 sunclock is an X11 application that displays a map of the Earth and
 indicates the illuminated portion of the globe by drawing sunlit
 areas dark on light, night areas as light on dark.  In addition to
 providing local time for the default timezone, it also displays GMT
 time, legal and solar time of major cities, their latitude and
 longitude, and the mutual distances of arbitrary locations on Earth.
 Sunclock can display meridians, parallels, tropics and arctic
 circles.  It has builtin functions that accelerate the speed of time
 and show the evolution of seasons.

Package: sunclock-maps
Architecture: all
Depends: ${misc:Depends}
Breaks: sunclock (<< 3.50pre1-3)
Replaces: sunclock (<< 3.50pre1-3)
Multi-Arch: foreign
Description: sunclock vector graphic maps
 sunclock is an X11 application that displays a map of the Earth and
 indicates the illuminated portion of the globe by drawing sunlit
 areas dark on light, night areas as light on dark.  In addition to
 providing local time for the default timezone, it also displays GMT
 time, legal and solar time of major cities, their latitude and
 longitude, and the mutual distances of arbitrary locations on Earth.
 .
 This package contains the vector graphic earthmaps.
